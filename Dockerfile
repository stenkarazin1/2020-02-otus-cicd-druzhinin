FROM openjdk:8-alpine
COPY helloworld.java .
RUN javac helloworld.java
RUN uname -a
RUN ls -la
CMD ["java", "HelloWorld"]
